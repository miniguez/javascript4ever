defmodule Wheelchair.WorkerTest do
  use Wheelchair.ModelCase

  alias Wheelchair.Worker

  @valid_attrs %{account: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Worker.changeset(%Worker{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Worker.changeset(%Worker{}, @invalid_attrs)
    refute changeset.valid?
  end
end
