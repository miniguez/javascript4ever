defmodule Wheelchair.TaskTest do
  use Wheelchair.ModelCase

  alias Wheelchair.Task

  @valid_attrs %{image_url: "some content", payment: 42, status: "some content", text: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Task.changeset(%Task{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Task.changeset(%Task{}, @invalid_attrs)
    refute changeset.valid?
  end
end
