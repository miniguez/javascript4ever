defmodule Wheelchair.WorkerControllerTest do
  use Wheelchair.ConnCase

  alias Wheelchair.Worker
  @valid_attrs %{account: "some content", name: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, worker_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    worker = Repo.insert! %Worker{}
    conn = get conn, worker_path(conn, :show, worker)
    assert json_response(conn, 200)["data"] == %{"id" => worker.id,
      "name" => worker.name,
      "account" => worker.account}
  end

  test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, worker_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, worker_path(conn, :create), worker: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Worker, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, worker_path(conn, :create), worker: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    worker = Repo.insert! %Worker{}
    conn = put conn, worker_path(conn, :update, worker), worker: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Worker, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    worker = Repo.insert! %Worker{}
    conn = put conn, worker_path(conn, :update, worker), worker: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    worker = Repo.insert! %Worker{}
    conn = delete conn, worker_path(conn, :delete, worker)
    assert response(conn, 204)
    refute Repo.get(Worker, worker.id)
  end
end
