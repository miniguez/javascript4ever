ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Wheelchair.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Wheelchair.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Wheelchair.Repo)

