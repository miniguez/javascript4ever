defmodule Wheelchair.Repo.Migrations.CreateTask do
  use Ecto.Migration

  def change do
    create table(:tasks) do
      add :image_url, :string
      add :text, :text
      add :payment, :integer
      add :status, :string
      add :worker_id, references(:workers, on_delete: :nothing)

      timestamps
    end
    create index(:tasks, [:worker_id])

  end
end
