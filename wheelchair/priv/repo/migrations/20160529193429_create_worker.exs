defmodule Wheelchair.Repo.Migrations.CreateWorker do
  use Ecto.Migration

  def change do
    create table(:workers) do
      add :name, :string
      add :account, :string
      add :password, :string
      add :email, :string
      timestamps
    end

  end
end
