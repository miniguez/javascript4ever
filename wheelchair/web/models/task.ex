defmodule Wheelchair.Task do
  use Wheelchair.Web, :model
  import Ecto.Query

  schema "tasks" do
    field :image_url, :string
    field :text, :string
    field :payment, :integer
    field :status, :string
    belongs_to :worker, Wheelchair.Worker

    timestamps
  end

  @required_fields ~w(image_url text payment status worker_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def last(query) do
       from(t in query,
       where: t.worker_id ==1 and t.status == "created",
       order_by: [desc: t.inserted_at],
       limit: 1)
  end

  def getHistory(id) do
      from(
          t in "tasks",
          where: t.worker_id == ^id,
          select: {t.id, t.worker_id, t.image_url, t.text, t.payment, t.status}
      )
  end

end
