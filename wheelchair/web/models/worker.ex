defmodule Wheelchair.Worker do
  use Wheelchair.Web, :model
  import Ecto.Query

  schema "workers" do
    field :name, :string
    field :account, :string
    field :password, :string
    field :email, :string
    has_many :tasks, Wheelchair.Task

    timestamps
  end
  balance=0

  @required_fields ~w(name account password email)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def getBalance(worker_id) do
      from(w in "workers",
      join: t in "tasks", on: t.worker_id == w.id,
      select: sum(t.payment),
      where: t.status == "done" and t.worker_id == ^worker_id
      )
  end

end
