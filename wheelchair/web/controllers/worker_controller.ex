defmodule Wheelchair.WorkerController do
  use Wheelchair.Web, :controller

  alias Wheelchair.Worker

  plug :scrub_params, "worker" when action in [:create, :update]

  def index(conn, _params) do
    workers = Repo.all(Worker)
    render(conn, "index.json", workers: workers)
  end

  def create(conn, %{"worker" => worker_params}) do
    changeset = Worker.changeset(%Worker{}, worker_params)

    case Repo.insert(changeset) do
      {:ok, worker} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", worker_path(conn, :show, worker))
        |> render("show.json", worker: worker)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Wheelchair.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    worker = Repo.get!(Worker, id)
    render(conn, "show.json", worker: worker)
  end

  def update(conn, %{"id" => id, "worker" => worker_params}) do
    worker = Repo.get!(Worker, id)
    changeset = Worker.changeset(worker, worker_params)

    case Repo.update(changeset) do
      {:ok, worker} ->
        render(conn, "show.json", worker: worker)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Wheelchair.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    worker = Repo.get!(Worker, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(worker)

    send_resp(conn, :no_content, "")
  end

  def balance(conn, %{"id" => id}) do
    id = :string.to_integer(to_char_list(id))
    balance = Repo.one(Worker.getBalance(elem(id, 0)))
    if balance == nil do
        balance = 0
    end
    render(conn, "balance.json", balance: balance)
  end

end
