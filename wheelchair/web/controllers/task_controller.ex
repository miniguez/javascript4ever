defmodule Wheelchair.TaskController do
  use Wheelchair.Web, :controller

  alias Wheelchair.Task

  plug :scrub_params, "task" when action in [:create, :update]

  def index(conn, _params) do
    tasks = Repo.all(Task)
    render(conn, "index.json", tasks: tasks)
  end

  def create(conn, %{"task" => task_params}) do
    changeset = Task.changeset(%Task{}, task_params)

    case Repo.insert(changeset) do
      {:ok, task} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", task_path(conn, :show, task))
        |> render("show.json", task: task)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Wheelchair.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    task = Repo.get!(Task, id)
    render(conn, "show.json", task: task)
  end

  def update(conn, %{"id" => id, "task" => task_params}) do
    task = Repo.get!(Task, id)
    changeset = Task.changeset(task, task_params)

    case Repo.update(changeset) do
      {:ok, task} ->
        render(conn, "show.json", task: task)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Wheelchair.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    task = Repo.get!(Task, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(task)

    send_resp(conn, :no_content, "")
  end

  def latest(conn, _params) do
      task = Repo.one(Task.last(Task))
      render(conn, "show.json", task: task)
  end

  def history(conn, %{"id" => id}) do
    id = :string.to_integer(to_char_list(id))
    #tasks = Repo.all(Task.getHistory(elem(id, 0)))
    query = from t in Task,
         select: {t.id, t.worker_id, t.image_url, t.text, t.payment, t.status},
         where: t.worker_id == ^elem(id, 0)
    tasks = Repo.all(query)
    render(conn, "history.json", tasks: tasks)
  end
end
