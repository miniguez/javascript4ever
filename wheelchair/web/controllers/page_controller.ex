defmodule Wheelchair.PageController do
  use Wheelchair.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
