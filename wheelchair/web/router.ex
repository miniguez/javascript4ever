defmodule Wheelchair.Router do
  use Wheelchair.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Wheelchair do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", Wheelchair do
     pipe_through :api
     get "/workers/:id/balance", WorkerController, :balance
     get "/tasks/:id/history", TaskController, :history
     resources "/workers", WorkerController, except: [:new, :edit]
     get "/tasks/latest", TaskController, :latest
     resources "/tasks", TaskController
  end

end
