defmodule Wheelchair.WorkerView do
  use Wheelchair.Web, :view

  def render("index.json", %{workers: workers}) do
    %{data: render_many(workers, Wheelchair.WorkerView, "worker.json")}
  end

  def render("show.json", %{worker: worker}) do
    %{data: render_one(worker, Wheelchair.WorkerView, "worker.json")}
  end

  def render("worker.json", %{worker: worker}) do
    %{id: worker.id,
      name: worker.name,
      account: worker.account}
  end

  def render("balance.json", %{balance: balance}) do
      %{
        balance: balance
      }
  end


end
