defmodule Wheelchair.TaskView do
  use Wheelchair.Web, :view

  def render("index.json", %{tasks: tasks}) do
    %{data: render_many(tasks, Wheelchair.TaskView, "task.json")}
  end

  def render("show.json", %{task: task}) do
    %{data: render_one(task, Wheelchair.TaskView, "task.json")}
  end

  def render("history.json", %{tasks: tasks}) do
      %{data: render_many(tasks, Wheelchair.TaskView, "task.json")}
  end

  def render("task.json", %{task: task}) do
    %{id: task.id,
      worker_id: task.worker_id,
      image_url: task.image_url,
      text: task.text,
      payment: task.payment,
      status: task.status}
  end
end
